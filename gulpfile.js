// Gulp
const { src, dest, watch, series, parallel } = require("gulp"),
  browserSync = require("browser-sync").create(),
  fileInclude = require("gulp-file-include"),
  urlPrefixer = require("gulp-url-prefixer"),
  sass = require("gulp-sass"),
  mergeMQ = require("gulp-merge-media-queries"),
  cssMin = require("gulp-csso"),
  beautify = require("gulp-jsbeautifier");

// Webpack
const webpack = require("webpack"),
  webpackDevMiddleware = require("webpack-dev-middleware"),
  webpackHotMiddleware = require("webpack-hot-middleware"),
  webpackConfig = require("./webpack.config"),
  bundler = webpack(webpackConfig);

// pathURL
const dirURL = "sorocabacom",
  srcURL = "./src/",
  distURL = "./dist/",
  watchURL = "./" + dirURL + "/dist/",
  webURL = "https://projectNAME.com/";

const path = {
  build: {
    html: distURL,
    css: distURL + "assets/css/",
  },
  src: {
    html: [srcURL + "*.html", "!" + srcURL + "_*.html"],
    sass: srcURL + "assets/sass/style.scss",
  },
  watch: {
    html: srcURL + "*.html",
    sass: srcURL + "assets/sass/*.scss",
    js: srcURL + "assets/js/*.js",
    php: distURL + "*.php",
  },
};

// Server
function serverSync() {
  browserSync.init({
    open: "external",
    port: 9000,
    proxy: "localhost" + watchURL,
    middleware: [
      webpackDevMiddleware(bundler, {
        publicPath: webpackConfig.output.publicPath,
        stats: { colors: true },
      }),
      webpackHotMiddleware(bundler),
    ],
  });
}
function reload(cb) {
  browserSync.reload();
  cb();
}

// Develop
function html(cb) {
  src(path.src.html)
    .pipe(fileInclude({ prefix: "@@" }))
    .pipe(dest(path.build.html));
  cb();
}
function css(cb) {
  src(path.src.sass)
    .pipe(sass().on("error", sass.logError))
    .pipe(mergeMQ({ log: true }))
    .pipe(cssMin())
    // .pipe(beautify())
    .pipe(dest(path.build.css))
    .pipe(browserSync.stream());
  cb();
}
function js(cb) {
  return webpack(webpackConfig, function (err, stats) {
    if (err) throw err;
    console.log(stats.toString());
    cb();
  });
}

// Build
function publicHtml(cb) {
  src(path.build.html + "*.html")
    .pipe(urlPrefixer.html({ prefix: webURL }))
    .pipe(dest(path.build.html));
  cb();
}
function publicCss(cb) {
  src(path.build.css + "style.css")
    .pipe(cssMin())
    .pipe(dest(path.build.css));
  cb();
}

// Watch
function watchFiles() {
  watch(path.watch.html, series(html, reload));
  watch(path.watch.sass, series(css));
  watch(path.watch.js, series(js, reload));
  watch(path.watch.php, series(reload));
}

// Task
exports.develop = series(html, css, js, parallel(serverSync, watchFiles));
exports.build = series(publicHtml, publicCss);
exports.watch = parallel(serverSync, watchFiles);
