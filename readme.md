
# Lorenze Front Boilerplate (LFB)

Standard coding structure **Lorenze**

## Setup

* NODE
* GULP
* WEBPACK

<!-- npm install gulp --save-dev -->
npm i browser-sync -D
npm i babel-loader -D
npm i @babel/core -D
npm i @babel/preset-env -D

npm i gulp -D
npm i gulp-file-include -D
npm i gulp-url-prefix -D
npm i gulp-sass -D
npm i gulp-merge-media-queries -D
npm i gulp-csso -D
npm i gulp-jsbeautifier -D

npm i webpack -D
npm i webpack-dev-middleware -D
npm i webpack-hot-middleware -D
