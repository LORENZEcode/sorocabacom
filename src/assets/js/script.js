// HRM
if (module.hot) {
  module.hot.accept();
}

// Embla Carousel

import EmblaCarousel from "embla-carousel";
import { setupPrevNextBtns, disablePrevNextBtns } from "./prevAndNextButtons";

const wrap = document.querySelector(".embla");
const viewPort = wrap.querySelector(".embla-viewport");
const prevBtn = wrap.querySelector(".embla-button-prev");
const nextBtn = wrap.querySelector(".embla-button-next");
const embla = EmblaCarousel(viewPort, {
  loop: true,
  align: "start",
  containScroll: "trimSnaps",
});
const disablePrevAndNextBtns = disablePrevNextBtns(prevBtn, nextBtn, embla);

setupPrevNextBtns(prevBtn, nextBtn, embla);

embla.on("select", disablePrevAndNextBtns);
embla.on("init", disablePrevAndNextBtns);

// Form

import Swal from 'sweetalert2'

document.querySelector("form").addEventListener("submit", event => {

  Swal.fire(
    'Sucesso!',
    'Mensagem enviada com sucesso',
    'success'
  )
  event.preventDefault()
})
