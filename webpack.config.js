const path = require("path"),
  webpack = require("webpack");

module.exports = {
  entry: {
    main: [
      // "webpack/hot/dev-server",
      "webpack-hot-middleware/client",
      "./src/assets/js/script.js",
    ],
  },

  output: {
    filename: "script.js",
    path: path.resolve(__dirname, "./dist/assets/js"),
    publicPath: "http://192.168.0.105:9000/dist",
  },

  watch: false,
  mode: "production",

  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
    ],
  },

  plugins: [new webpack.HotModuleReplacementPlugin()],
};
